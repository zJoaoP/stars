package solutis;

import java.awt.geom.Point2D;
import java.awt.geom.Line2D;
import java.awt.Graphics2D;
import java.awt.Color;
import robocode.*;

import robocode.util.Utils;

public class Stars extends AdvancedRobot{	
	private static final double BULLET_EVASION_DISTANCE = 700.0;
	private static final double MOVEMENT_OFFSET = 200.0;
	private static final double WALL_DISTANCE = 90;
	private static final int BULLET_COUNT = 30;
	
	private double[][] bullets = new double[BULLET_COUNT][6]; //x, y, bearing, speed, time, valid
	private int bIndex = 0;
	
	private double currentEnemyEnergy = Double.POSITIVE_INFINITY;
	private double enemyX, enemyY;
	
	public void run() {
		setTurnRadarRightRadians(Double.POSITIVE_INFINITY);
	}

	public void onPaint(Graphics2D g) {
	    for(int i = 0; i < BULLET_COUNT; i++){
			if(bullets[i][5] < 1.0)
				continue;
			
			int x = getBulletX(i), y = getBulletY(i);
			
			g.setColor(Color.RED);
			g.fillOval(x, y, 5, 5);

			g.drawLine(x, y, (int) (x + 500 * Math.sin(bullets[i][2])), (int) (y + 500 * Math.cos(bullets[i][2])));
		}
	}
	
	public void onEnemyFire(double bearing, double distance, double power){		
		double x = enemyX - Math.sin(bearing) * getWidth() / 2;
		double y = enemyY - Math.cos(bearing) * getWidth() / 2;
				
		// Register bullet on current location.
		registerPossibleBullet(x, y, Math.atan2(getX() - x, getY() - y), Rules.getBulletSpeed(power));

		// Register bullet on future location. TODO
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		double absoluteBearing = getHeadingRadians() + e.getBearingRadians();

		enemyX = getX() + Math.sin(absoluteBearing) * e.getDistance();
		enemyY = getY() + Math.cos(absoluteBearing) * e.getDistance();
		
		// --------------------------- Radar ---------------------------
		setTurnRadarLeftRadians(getRadarTurnRemainingRadians());
		
		// --------------------------- Bullet Tracker ---------------------------
		for(int i = 0; i < BULLET_COUNT; i++){
			if(bullets[i][5] < 1.0)
				continue;
			
			double x = getBulletX(i);
			double y = getBulletY(i);
			if(x < 0 || y < 0)
				bullets[i][5] = 0.0;
			else if(x > getBattleFieldWidth() || y > getBattleFieldHeight())
				bullets[i][5] = 0.0;
		}
		
		if(currentEnemyEnergy == Double.POSITIVE_INFINITY)
			currentEnemyEnergy = e.getEnergy();
		else if(currentEnemyEnergy != e.getEnergy()){ // Energy has changed.
			if((e.getEnergy() < currentEnemyEnergy) && (currentEnemyEnergy - e.getEnergy() <= 3.0))
				this.onEnemyFire(absoluteBearing, e.getDistance(), currentEnemyEnergy - e.getEnergy());
				
			currentEnemyEnergy = e.getEnergy();
		}
		// --------------------------- Movement ---------------------------
		Point2D dest = getMinimumRiskPosition();
		
		double angle = Utils.normalAbsoluteAngle(Math.atan2(dest.getX() - getX(), dest.getY() - getY()));
		double distance = dest.distance(getX(), getY());
		
		if(Math.abs(angle - getHeadingRadians()) < Math.PI / 2) {
			setTurnRightRadians(Utils.normalRelativeAngle(angle - getHeadingRadians()));
			setAhead(distance);
		}
		else {
			setTurnRightRadians(Utils.normalRelativeAngle(angle + Math.PI - getHeadingRadians()));
			setAhead(-distance);
		}

		// --------------------------- Aiming ---------------------------
	}
	
	public void registerPossibleBullet(double x, double y, double bearing, double speed){
		bullets[bIndex] = new double []{x, y, bearing, speed, this.getTime(), 1.0};
		bIndex = (bIndex + 1) % BULLET_COUNT;
	}
	
	public int getBulletX(int index){
		return (int) (bullets[index][0] + Math.sin(bullets[index][2]) * bullets[index][3] * (getTime() - bullets[index][4]));
	}
	
	public int getBulletY(int index){
		return (int) (bullets[index][1] + Math.cos(bullets[index][2]) * bullets[index][3] * (getTime() - bullets[index][4]));
	}
	
	public double risk(Point2D p){
		double riskFactor = 0.0;
		
		for(int i = 0; i < BULLET_COUNT; i++){
			if(bullets[i][5] < 1.0)
				continue;

			double x = getBulletX(i), y = getBulletY(i);
			
			Line2D bulletTrajectory = new Line2D.Double(x,
														y,
														x + BULLET_EVASION_DISTANCE * Math.sin(bullets[i][2]),
														y + BULLET_EVASION_DISTANCE * Math.cos(bullets[i][2]));
			
			riskFactor -= bulletTrajectory.ptLineDist(p);
		}
		riskFactor -= p.distance(enemyX, enemyY);
		return riskFactor;
	}
	
	public Point2D getMinimumRiskPosition(){
		double bestRisk = Double.POSITIVE_INFINITY;
		Point2D best = null;
		
		for(int i = -1; i <= 1; i++){
			for(int j = -1; j <= 1; j++){
				if(i == 0 && j == 0)
					continue;
					
				double pointX = Math.max(WALL_DISTANCE, Math.min(getBattleFieldWidth() - WALL_DISTANCE, getX() + i * MOVEMENT_OFFSET));
				double pointY = Math.max(WALL_DISTANCE, Math.min(getBattleFieldHeight() - WALL_DISTANCE, getY() + j * MOVEMENT_OFFSET));
				
				Point2D p = new Point2D.Double(pointX, pointY);
				double currentRisk = risk(p);
				if(currentRisk < bestRisk){
					best = p;
					bestRisk = currentRisk;
				}
			}
		}
		return best;
	}
}
